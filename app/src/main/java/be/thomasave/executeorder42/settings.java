package be.thomasave.executeorder42;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.widget.TextView;
import android.widget.Toast;

public class settings extends PreferenceActivity{
    public static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settingsactivity);
        addPreferencesFromResource(R.xml.preferences);
        final SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String sender = sharedpreferences.getString("sender",null);
        EditTextPreference editTextPreference = (EditTextPreference) findPreference("email");
        editTextPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                Toast.makeText(getBaseContext(), "Recipient email successfully changed", Toast.LENGTH_LONG).show();
                return true;
            }
        });
        TextView textView = (TextView) findViewById(R.id.textview);
        textView.setText("Your email is " + sender);
    }
}